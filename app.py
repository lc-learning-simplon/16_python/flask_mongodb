from flask import Flask, url_for, request, render_template, redirect
from flask_pymongo import PyMongo
from bson.objectid import ObjectId

app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://localhost:27017/ToDoList'
mongo = PyMongo(app)

@app.route('/')
def index():
    # Render template home and show tasks collection.
    tasks = mongo.db.tasks.find({})
    return render_template('home.html', tasks=tasks)

@app.route('/submit', methods=['GET', 'POST'])
def submit():
    # Store task in tasks collection in database ToDoList.
    task = request.args.get('task')
    data = {'task': task}
    mongo.db.tasks.insert_one(data)
    return redirect("/")

@app.route('/delete/<task_id>')
def delete(task_id):
    # Delete the task with her _id and redirect tout home.
    mongo.db.tasks.delete_one({'_id': ObjectId(task_id)})
    return redirect('/')

