# Flask & MongoDB ToDoList

## Quickstart
`. venv/bin/activate`

`export FLASK_APP=app.py`

`export FLASK_ENV=development`

`pip3 install`

`flask run`

**Need mongoDB local.**

`sudo systemctl restart mongod`
